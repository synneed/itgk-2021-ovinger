# Oppgave fra Insperaøving: Float v2

# Som nevnt tidligere har ikke python en isfloat(s) funksjon. Senere skal dere lære om feilhåndering med try og except.
# Forklar i tekst eller kode hvordan man kan bruke try og except som hjelp til å konvertere strings til flyttall:

def to_float(tekst):
    try:
        return float(tekst)
    except ValueError:
        print("Noe gikk galt")
        return None

# print(to_float("hey"))


# Oppgave fra Insperaøving: Værmelding v2

# I tidligere oppgaver hadde værmeldingene denne formen: [["mandag", "sol"],
# ["tirsdag", "sol"], ["onsdag", "regn"], ["torsdag", "overskyet"], ["fredag", "regn"],
# ["lørdag", "regn"], ["søndag", "overskyet"]].

# En annen måte å representere en ukes værmelding er ved å bruke dictionaries.
# Lag en funksjon som tar inn en liste på den formen vi har brukt før, 
# og returner en dictionary som kan aksessere været ved ukedager som nøkkel. 
# Fremdeles antar vi at vi ikke har flere av samme dag i listen.

ukedager = [
    ["mandag", "sol"], 
    ["tirsdag", "sol"], 
    ["onsdag", "regn"], 
    ["torsdag", "overskyet"], 
    ["fredag", "regn"], 
    ["lørdag", "regn"], 
    ["søndag", "overskyet"]
]

def to_dict(liste):
    ukedager_dict = {}
    for line in liste:
        ukedager_dict[line[0]] = line[1]
    return ukedager_dict

# print(to_dict(ukedager))


# Oppgave fra Insperaøving: Read/Write fil

# Dere husker kanskje listen weather_data: [["sol", 2], ["regn", 13], ["vind", 5],
# ["torden", 1]]  fra oppgave 3.

# Lag en funksjon som tar inn værdata og lagrer dette i en tekstfil.
# I tillegg skal du lage en funksjon for å lese denne tekstfilen og hente ut data på
# samme format som det var før det ble lagret. Formatet på dataen kan enten være som før, 
# eller dere kan lage en dictionary i stedet, ved å for eksempel bruke funksjonen fra oppgave 5.1. 
# Husk å spesifisere formatet på dataen (kan gjøres i en kommentar f.eks.).

weather_data = [
    ["sol", 2], 
    ["regn", 13], 
    ["vind", 5], 
    ["torden", 1]
]

def write(weather_data):
    with open("file.txt", "w") as file:
        for line in weather_data:
            file.write(f"{line[0]},{line[1]}\n")

#write(weather_data)



# Oppgave 3
def factorial(n):
    if n == 1:
        return 1
    else:
        return n * factorial(n-1)

#print(factorial(5))


# Oppgave 4
def binomial(n, k):
    return factorial(n) / (factorial(k) * factorial(n - k))

# print(binomial(5, 4))

liste = [[4,5,1], 5, 2, 8, [2], [21, [9123,[23] ,3], 2]]


# Oppgave 5
def sum_list(liste):
    s = 0
    for elem in liste:
        if isinstance(elem, list):
            s += sum_list(elem)
        else:
            s += elem
    return s

print(sum_list(liste))