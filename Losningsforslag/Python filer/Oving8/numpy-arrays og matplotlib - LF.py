#!/usr/bin/env python
# coding: utf-8

# ## Løsningsforslag

# In[ ]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np

# Oppgave a
array = np.arange(0.0, 30.0, 0.1)
sin_array = np.sin(array)
plt.plot(array, sin_array)
plt.show()


# Oppgave b
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

def double_sine_plot(freq_mult):
    array = np.arange(0.0, 15.0, 0.1)
    sin_array = np.sin(array)
    other_sin_array = np.sin(array*freq_mult)
    
    plt.plot(array, array*0, "k-")
    plt.plot(array, sin_array, "c:")
    plt.plot(array, other_sin_array, "y:")
    plt.plot(array, sin_array + other_sin_array, "r-")
    plt.title('To sinus-kurver plusset sammen')
    plt.show()
    
double_sine_plot(2)

def double_sine_plot(freq_mult, amp_mult):
    array = np.arange(0.0, 15.0, 0.1)
    sin_array = np.sin(array)
    other_sin_array = np.sin(array*freq_mult) * amp_mult
    
    plt.plot(array, array*0, "k-")
    plt.plot(array, sin_array, "c:")
    plt.plot(array, other_sin_array, "y:")
    plt.plot(array, sin_array + other_sin_array, "r-")
    plt.title('To sinus-kurver plusset sammen')
    plt.show()
    
double_sine_plot(12,0.3)

